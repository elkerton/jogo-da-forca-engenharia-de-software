(function(){
    var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
    $.ajax({
        url : '/forcaUlbra/multiplayer/'+id+'',
        type: 'POST',
        dataType : 'json',
        contentType : 'application/json',
        success : function(data){
            console.log(data)
            removeContent(data);
        },
        error : function(data){
            console.log(data.error)
        }

    })
})();
function removeContent(data){
    $('.content').addClass('content-hide');
    $('.content').removeClass('content');
    criarJogo(data)
}

function mostrarJogo(data){
    $('body').css('background', 'none')
    $('.pontos').show()
    $('.jogo h1').text('Dica: '+ data.dica+'')
    $('.jogo').addClass('jogo-show')
    $('.jogo').removeClass('jogo')
    if(!setUsuario){
        $('.modal').show()
        $('.usuario').show()
        setUsuario = 1;
    }
    dataPalavra = data.palavra.toUpperCase();
    if(dataPalavra.indexOf(' ') == -1){
         tipoJogo = 1;
        /*Tipo 1 = palavra*/
    }else{
         tipoJogo = 2;
        /*Tipo 2 = Frase*/
    }
}

function criarJogo(data){
    console.log('oi')
    //Montar Underlines
    var palavraUnderline = '';

    for(var i = 0; i < data.palavra.length; i++){
        if(data.palavra.charAt(i) != ' '){
            palavraUnderline = palavraUnderline + '_' 
        }else{
            palavraUnderline = palavraUnderline + ' ';
        }
    }

    $('.linhas').text(palavraUnderline)
    // Fim Montar Underlines

    //Montar Teclado
    var alfabeto = 'abcdefghijklmnopqrstuvwxyz';
    var alfabetoArray = [];

    for(var i = 0; i < alfabeto.length; i++){
        alfabetoArray.push(alfabeto.charAt(i).toUpperCase())
    }
    //Fim Montar Teclado
    alfabetoArray.forEach(function(item) {
        $('.letras').append("<button class='btn-forca' onclick='validaLetra(event)'>"+item+"</button>")
    }, this);
    mostrarJogo(data)
}

$('.btn-reiniciar').click(function(){
    location.href = 'http://localhost:3000/forcaUlbra/home'
})