var mongoose = require('mongoose')

module.exports = function(app){
    var dbPalavras = mongoose.model('Singleplayer') 
    var dbCategorias = mongoose.model('Categoria') 

    app.post('/forcaUlbra/criar/palavra', function(req, res){
        if(req.body.tipo == 'categoria'){
            var item = new dbCategorias({
                categoria : req.body.palavraOuFrase,
                tipo : 'palavra'
            })
            var item1 = new dbCategorias({
                categoria : req.body.palavraOuFrase,
                tipo : 'frase'
            })

            dbCategorias.find({categoria : req.body.palavraOuFrase}).exec((err, data)=>{
                if(!err){
                    if(data.length > 0){
                        res.json({message : "Categoria Já Existe"})
                    }else{
                        item.save(function(err, data){
                            if(!err){
                                item1.save(function(err, data){
                                    if(!err){   
                                        res.json({message : "Categoria Salva com Sucesso"})
                                    }else{
                                        res.json({error : "Erro, contate o administrador"})
                                    }
                                })
                            }else{
                                res.json({error : "Erro, contate o administrador"})
                            }
                        })
                    }
                }else{
                    res.json({error : "Erro, contate o administrador"})
                }
            })
        }else{
            var item = new dbPalavras(req.body)

            dbPalavras.find({palavraOuFrase : req.body.palavraOuFrase, categoria : req.body.categoria, tipo : req.body.tipo}).exec((err, data)=>{
                if(!err){
                    console.log(data)
                    if(data.length > 0){
                        res.json({message : "Palavra Já Existe"})
                    }else{
                        item.save(function(err, data){
                            if(!err){
                                res.json({message : "Salvo com Sucesso"})
                            }else{
                                res.json({error : "Erro, contate o administrador"})
                            }
                        })
                    }
                }else{
                    res.json({error : "Erro, contate o administrador"})
                }
            })

            
        }
        
         
    })
}