
var mongoose = require('mongoose')

module.exports = function(app){
    var db = mongoose.model('Multiplayer')
    app.post('/forcaUlbra/multiplayer', function(req, res){
        
        var item = new db({
            palavra  : req.body.palavra,
            dica : req.body.dica,
            email : req.body.email
        })

        item.save(function(err, data){
            if(!err){
                console.log(data)
                res.json({data : data, error : null})
            }else{
                res.json({data : null, error: "Ocorreu algum erro ao criar o Jogo"})
            }
        })
        
    })
}