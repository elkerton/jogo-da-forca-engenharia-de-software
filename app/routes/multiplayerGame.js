
var mongoose = require('mongoose')
var nodemailer = require('nodemailer')
module.exports = function(app){
    var db = mongoose.model('Multiplayer')
    app.get('/forcaUlbra/multiplayer/:id', function(req, res){
        var id = req.params.id;
        id = mongoose.Types.ObjectId(id);
        

        db.find({_id : id }).exec((err, data) =>{
            if(!err){
                console.log(data)
                res.render('multiplayer', {data : data})
            }else{
                res.json({data : null, error: "Ocorreu algum erro ao criar o Jogo"})
            }
        })
        
    })

    app.post('/forcaUlbra/multiplayer/:id', function(req, res){
        var id = req.params.id;
        id = mongoose.Types.ObjectId(id);
        var mensagem = ''

        db.find({_id : id }).exec((err, data) =>{
            if(!err){
                if(req.body.email){
                    if(req.body.ganhou){
                        mensagem = 'O Seu Adversário ' + req.body.usuario + ' Venceu o seu Desafio do Jogo da Forca!' 
                    }else{
                        mensagem = 'O Seu Adversário ' + req.body.usuario + ' Perdeu o seu Desafio do Jogo Da Forca!' 
                    }
                    let transporter = nodemailer.createTransport({
                        auth: {
                            user: 'testejogo@eagleapp.com.br',
                            pass: '123QWEq23!@#123qwe!@#'
                        },
                        host : 'mail44.redehost.com.br',
                        secure : false
                    });
                    let mailOptions = {
                        from: 'testejogo@eagleapp.com.br',
                        to: data[0].email, // list of receivers
                        subject: 'Jogo Da Forca', // Subject line
                        text: mensagem , // plain text body

                    };
                    transporter.sendMail(mailOptions, (error, info) => {
                    return res.json({win : req.body.ganhou});
                if (error) {
                    return console.log(error);
                }
                console.log('Message %s sent: %s', info.messageId, info.response);
            });
                }else{
                    console.log(data)
                    res.json({palavra : data[0].palavra, email : data[0].email, dica : data[0].dica})
                }
            }else{
                res.json({data : null, error: "Ocorreu algum erro ao criar o Jogo"})
            }
        })
        
    })
}