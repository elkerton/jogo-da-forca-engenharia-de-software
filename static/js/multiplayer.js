

$('#criar-jogo-multiplayer').click(function(){
    var palavra = $('.multiplayer-construct input[name="palavra-multiplayer"]').val();
    var dica = $('.multiplayer-construct input[name="dica"]').val();
    var email = $('.multiplayer-construct input[name="email"]').val();
    if( palavra != '' && dica != '' && email != '' ){
        var data = {
            palavra : palavra,
            dica : dica,
            email : email
        }

        $.ajax({
        url : '/forcaUlbra/multiplayer',
        type: 'POST',
        dataType : 'json',
        contentType : 'application/json',
        data : JSON.stringify(data),
        success : function(data){
            console.log(data.data._id)
            $('.multiplayer-construct div').remove();
            $('.multiplayer-construct').append('<div class="row"><h2 class="text-center">Link do Jogo</h2><input type="text" class="col-md-12" value="http://localhost:3000/forcaUlbra/multiplayer/'+data.data._id+'"></div>');
        },
        error : function(data){
            console.log(data.error)
        }

    })
    }else if($('.multiplayer-construct #error-message').length == 0){
        $('.multiplayer-construct').append('<p id="error-message" style="color:red;">Todos os Campos devem ser preenchidos</p>');
    }
})

