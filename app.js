var app = require('./config/express')()
require('./config/connect')

var port = process.env.PORT || 3000

app.listen(port, function(){
    console.log('Aplicação Online na porta : '  + port)
})