$('.options button').click(function(){
    var id = $(this).attr('id');
    $('.conteudo p').remove();
    $('.conteudo div:visible').eq(0).hide()
    $('.' + id).show();
})

$('.conteudo input[type="submit"]').click(function(){
    var typeCreate = $(this).attr('id').substring(6)
   
   if(typeCreate == 'frase' && $('.conteudo input[type="text"]:visible').val().indexOf(' ') == -1){
        if($('.conteudo p').length == 0){
            $('.conteudo').append('<p class="text-center" style="color:red">Digite uma Frase</p>')
        }
    }else{

   if($('.conteudo input:visible').val() == ''){
       
       if($('.conteudo p').length == 0){
                $('.conteudo').append('<p class="text-center" style="color:red">Preencha os Campos</p>')
            }

    }else{

        var data = {
            palavraOuFrase : $('.conteudo input:visible').val(),
            categoria : $('.conteudo select:visible').val(),
            tipo : typeCreate
        }
        console.log(data)
        $.ajax({
            url : '/forcaUlbra/criar/palavra',
            type: 'POST',
            dataType : 'json',
            contentType : 'application/json',
            data : JSON.stringify(data),
            success : function(data){
                $('.conteudo input[type="text"]:visible').val('')
                $('.conteudo p').remove()
                $('.conteudo').append('<p class="text-center" style="color:green">'+data.message+'</p>')
            },
            error : function(data){
                console.log(data.error)
            }

        })
   }
}
})