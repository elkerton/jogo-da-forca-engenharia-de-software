var dataPalavra = '';
var tipoJogo = null;
var pontos = 0;
var status = null; 
var setUsuario = 0;
var countLetters = 0; 

$('.reiniciar').click(function(){
    location.reload()
})
$('#visualizar-ranking').click(function(){
    console.log('teste')
    $.ajax({
        url : '/forcaUlbra/getRanking',
        type: 'GET',
        dataType : 'json',
        success : function(data){
            data.data.sort(function(a, b) {
                return (a.pontos) - (b.pontos);
            });
            data.data.reverse()

                for(var i = 0 ; i < 10; i++) {
                    $('.modal-final table tbody').append("<tr><td>"+data.data[i].player+"</td><td>"+ (i+1) +"</td><td>"+data.data[i].pontos+"</td></tr>")
                }
                $('.modal').show()
                $('.modal-parabens').hide()
                $('.modal-final  h2').hide()
                $('.modal-final').show()
        },
        error : function(data){
            console.log(data.error)
        }

    })
})

$('.body-content div').click(function(){
    if($(this).attr('id') == 'play-palavra'){
        $(this).removeClass('play-palavra')
        $(this).addClass('play-palavra-click')
        $('.x-btn').show()
        $('.list-categories-palavra').show()
    }else if($(this).attr('id') == 'play-frase'){
        $(this).removeClass('play-frase')
        $(this).addClass('play-frase-click')
        $('.x-btn').show()
        $('.list-categories-frase').show()
    }else if($(this).attr('id') == 'play-both'){
        $(this).removeClass('play-both')
        $(this).addClass('play-both-click')
        $('.x-btn').show()
        $('.list-categories-fraseepalavra').show()
    }
})

$('.x-btn').click(function(){  
    console.log(document.querySelector('.play-palavra-click')) 
    if(document.querySelector('.play-palavra-click') != null){
        $('#play-palavra').removeClass('play-palavra-click')
        $('#play-palavra').addClass('play-palavra')
        $(this).hide()    
        $('.list-categories-palavra').hide()
    }else if(document.querySelector('.play-frase-click') != null){
        $('#play-frase').removeClass('play-frase-click')
        $('#play-frase').addClass('play-frase')
        $(this).hide()
        $('.list-categories-frase').hide()
    }else if(document.querySelector('.play-both-click') != null){
        $('#play-both').removeClass('play-both-click')
        $('#play-both').addClass('play-both')
        $(this).hide()
        $('.list-categories-fraseepalavra').hide()
    }
    
})

$('div li button').click(function(){

    if($(this).attr('class') == 'both-id'){
        tipoJogo = 3;
    }
    var id  = $(this).attr('id');
    id = id.replace('iniciarJogo', '');

    var data = {
        id : id
    }

    $.ajax({
        url : '/forcaUlbra/iniciarJogo',
        type: 'POST',
        dataType : 'json',
        contentType : 'application/json',
        data : JSON.stringify(data),
        success : function(data){
            if(data.palavraOuFrase){
                console.log(data.palavraOuFrase)
                removeContent(data);
            }else{
                $(this).text(data.error)
            }
        }.bind(this),
        error : function(data){
            console.log(data.error)
        }

    })
})

$('.btn-proximo').click(function(){
    var categoria = $('.jogo-show h1').text().replace('Categoria ', '');
    var data = {
        categoria : categoria,
        tipo : tipoJogo
    }
    $.ajax({
        url : '/forcaUlbra/random',
        type: 'POST',
        dataType : 'json',
        contentType : 'application/json',
        data : JSON.stringify(data),
        success : function(data){
            if(data.palavraOuFrase){
                console.log(data.palavraOuFrase)
                limparJogo(data);
            }else if(data.message){
                setRanking(1)
            }else{
                console.log(error)
            }
        },
        error : function(data){
            console.log(data.error)
        }

    })
})

$('.usuario button').click(function(){
    $('.usuario').hide()
    $('.modal').hide()
})


function limparJogo(data){
    $('.linhas').text('');
    $('.letras').empty()
    $('.erros').text('')
    $('.modal').hide()
    criarJogo(data)
}




function validaLetra(event){
    var erros = '';
    var i = 0;
    var palavra = dataPalavra;
    var letra = $(event.target).text()
    var palavraUnderline = $('.linhas').text().split('');
    if(palavra.indexOf(letra) != -1){
        $(event.target).attr('disabled', 'disabled');
        $(event.target).addClass('btn-forca-active')
        while(palavra.indexOf(letra) != -1){
            palavraUnderline[palavra.indexOf(letra) + i ] = palavra.charAt(palavra.indexOf(letra))
            palavra = palavra.replace(letra,''); 
            i++;           
        } 

        palavraUnderline = palavraUnderline.toString();
        while(palavraUnderline.indexOf(',') != -1){
            palavraUnderline = palavraUnderline.replace(',','')
        }
        if(palavraUnderline == dataPalavra.toUpperCase()){
            if(tipoJogo == 1){
                pontos = pontos + 100;
                $('.pontos').text(pontos)
            }else if(tipoJogo == 2){
                pontos = pontos + 200;
                $('.pontos').text(pontos)
            }else if(tipoJogo == 3 && palavraUnderline.indexOf(' ') != -1){
                pontos = pontos + 200;
                $('.pontos').text(pontos)
            }else{
                pontos = pontos + 100;
                $('.pontos').text(pontos)
            }

            if($('.tipo-jogo').val() == 'multiplayer'){
                $('.modal').show();
                $('.modal-parabens-multiplayer').show()
                enviarEmail(1);
            }else{
                $('.modal').show();
                $('.modal-parabens').show()
            }
        }
        $('.linhas').text(palavraUnderline)
    }else{
        erros = $('.erros').text() + ' ' + letra
        $('.erros').text(erros)
        $(event.target).attr('disabled', 'disabled');
        $(event.target).addClass('btn-forca-disabled');
        if($('.erros').text().length == 10){
            $('.modal').show()
            var countPalavra = $('.linhas').text();
            if($('.tipo-jogo').val() == 'multiplayer'){
                while(countPalavra.indexOf('_') != -1){
                    countLetters++;
                    countPalavra = countPalavra.replace('_','')
                }
                while(countPalavra.indexOf(' ') != -1){
                    countLetters++;
                    countPalavra = countPalavra.replace(' ','')
                }
                pontos = pontos + ((palavraUnderline.length - countLetters) * 20)
                $('.pontos').text(pontos)
                enviarEmail(0)
            }else{
                while(countPalavra.indexOf('_') != -1){
                    countLetters++;
                    countPalavra = countPalavra.replace('_','')
                }
                while(countPalavra.indexOf(' ') != -1){
                    countLetters++;
                    countPalavra = countPalavra.replace(' ','')
                }
                pontos = pontos + ((palavraUnderline.length - countLetters) * 20)
                $('.pontos').text(pontos)
                setRanking(0)    
            }
        }
    }
    
}

function mostrarJogo(data){
    $('body').css('background', 'none')
    $('.pontos').show()
    $('.jogo h1').text('Categoria ' + data.categoria)
    $('.jogo').addClass('jogo-show')
    $('.jogo').removeClass('jogo')
    if(!setUsuario){
        $('.modal').show()
        $('.usuario').show()
        setUsuario = 1;
    }
    dataPalavra = data.palavraOuFrase.toUpperCase();
    if(tipoJogo != 3){
        if(dataPalavra.indexOf(' ') == -1){
         tipoJogo = 1;
        /*Tipo 1 = palavra*/
        }else{
            tipoJogo = 2;
            /*Tipo 2 = Frase*/
        }
    }
}

function setRanking(status){

    var data = {
        pontos : Number($('.pontos').text()),
        player : $('.usuario input').val()
    }

    $.ajax({
        url : '/forcaUlbra/setRanking',
        type: 'POST',
        dataType : 'json',
        contentType : 'application/json',
        data : JSON.stringify(data),
        success : function(data){
            data.data.sort(function(a, b) {
                return (a.pontos) - (b.pontos);
            });
            data.data.reverse()
            if(status){
                if(data.data.length < 10){
                    var rankingCount = data.data.length;
                }else{
                    var rankingCount = 10;
                }
                for(var i = 0 ; i < rankingCount; i++) {
                        $('.modal-final table tbody').append("<tr><td>"+data.data[i].player+"</td><td>"+ (i+1) +"</td><td>"+data.data[i].pontos+"</td></tr>")
                }
                $('.modal-parabens').hide()
                $('.modal-final').show()
            }else{
                if(data.data.length < 10){
                    var rankingCount = data.data.length;
                }else{
                    var rankingCount = 10;
                }
                for(var i = 0 ; i < rankingCount; i++) {
                    $('.modal-ranking table tbody').append("<tr><td>"+data.data[i].player+"</td><td>"+ (i+1) +"</td><td>"+data.data[i].pontos+"</td></tr>")
                }
                $('.modal-parabens').hide()
                $('.modal').show()
                $('.modal-ranking').show()
                console.log(data)
            }
        },
        error : function(data){
            console.log(data.error)
        }

    })

    var setUsuario = 0;
    
}

function criarJogo(data){
    //Montar Underlines
    var palavraUnderline = '';

    for(var i = 0; i < data.palavraOuFrase.length; i++){
        if(data.palavraOuFrase.charAt(i) != ' '){
            palavraUnderline = palavraUnderline + '_' 
        }else{
            palavraUnderline = palavraUnderline + ' ';
        }
    }

    $('.linhas').text(palavraUnderline)
    // Fim Montar Underlines

    //Montar Teclado
    var alfabeto = 'abcdefghijklmnopqrstuvwxyz';
    var alfabetoArray = [];

    for(var i = 0; i < alfabeto.length; i++){
        alfabetoArray.push(alfabeto.charAt(i).toUpperCase())
    }
    //Fim Montar Teclado
    alfabetoArray.forEach(function(item) {
        $('.letras').append("<button class='btn-forca' onclick='validaLetra(event)'>"+item+"</button>")
    }, this);
    mostrarJogo(data)
}

function removeContent(data){
    $('.content').addClass('content-hide');
    $('.content').removeClass('content');
    criarJogo(data)
}


$('#multiplayer').click(function(){
    $('.multiplayer-construct').show();
    $('.modal').show()
})

$('.fechar').click(function(){
    $('.multiplayer-construct').hide();
    $('.modal').hide();
})


function enviarEmail(status){
    var id = window.location.href.substring(window.location.href.lastIndexOf('/') + 1);
    $.ajax({
        url : '/forcaUlbra/multiplayer/'+id+'',
        type: 'POST',
        dataType : 'json',
        contentType : 'application/json',
        data : JSON.stringify({email : true, ganhou : status , usuario : $('.usuario input').val()}),
        success : function(data){
            console.log(data)
            if(data.win == 1){
                var mensagem = 'Você Ganhou!'            
            }else{
                var mensagem = 'Você Perdeu!'
            }
            $('.modal').show();
            $('.modal-parabens-multiplayer h2').remove()
            $('.modal-parabens-multiplayer').append('<h2>'+mensagem+'</h2>')
            $('.modal-parabens-multiplayer').show();
            removeContent(data);
        },
        error : function(data){
            console.log(data.error)
        }

    })
}



