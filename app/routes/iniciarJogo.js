var mongoose = require('mongoose')

module.exports = function(app){
    app.post('/forcaUlbra/iniciarJogo', function(req, res){
        var db = mongoose.model('Categoria');
        var dbJogo = mongoose.model('Singleplayer');
        var id = mongoose.Types.ObjectId(req.body.id);
        db.find(
                {
                    _id : id
                }
            ).exec((err, data)=>{
            if(!err){
                dbJogo.find(
                    {
                        $and : 
                        [
                            {
                                categoria : data[0].categoria
                            } ,
                            {
                                tipo : data[0].tipo
                            }
                        ]
                    }).exec((error, dataCategoria)=>{

                    if(!error){
                        if(dataCategoria[0]== null){
                            res.json({palavraOuFrase : null, error : 'Nenhuma Palavra foi encontrada!' })                                
                        }else{
                            var random = Math.floor(Math.random() * dataCategoria.length);
                            res.json({palavraOuFrase : dataCategoria[random].palavraOuFrase, categoria :dataCategoria[0].categoria,  error : null })                                
                        }
                    }else{
                        res.json({palavrabvOuFrase : null, error : "Não foi encontrada nenhuma palavra!"})
                    }
                })
            }else{
                res.json({palavraOuFrase : null, error : "Algum erro ocorreu, contate o Administrador!"})                
            }
        })

    })
}

