

var mongoose = require('mongoose')
module.exports = function(app){
    var db = mongoose.model('Ranking')
    app.post('/forcaUlbra/setRanking', function(req, res){
        
        var item = new db({
            player  : req.body.player,
            pontos : req.body.pontos
        })

        item.save(function(err){
            if(!err){
                db.find({}).exec((err, data)=>{
                    console.log(data)
                    res.json({data : data, error : null})
                })
            }else{
                res.json({data : null, error: "Ocorreu algum erro em salvar a sua pontuação!"})
            }
        })
        
    })
}